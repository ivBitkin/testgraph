
const express = require('express');
const graphQL = require('express-graphql');
const schema = require('./data/schema');
const graphql = require('graphql');


const app = express();


app.use('/graphql', graphQL({ schema:schema, pretty:true }));

app.listen(4000);

const fragment = 'fragment messageFragment on User { messages {text} }'

const query = 'query($id:String!) {user(id:$id) {name ...messageFragment}}'+fragment;

const values = {

    "id":"1"

};
graphql.graphql(schema, query, null, null, values).then(function (result) {
    console.log(JSON.stringify(result, null, " "));
});

