const users = {
    "1": {
        "id": "1",
        "name": "Hans",
        "messages":[1,2,3,4]
    },
    "2": {
        "id": "2",
        "name": "Maria",
        "messages":[1,2,3,4]

    },
    "3": {
        "id": "3",
        "name": "John",
        "messages":[1,2,3,4]

    }
};

const messages = {

    1:{ id:23, text:"Text 1" },
    2:{ id:23, text:"Text 2!" },
    3:{ id:23, text:"Text 3!" },
    4:{ id:23, text:"Text 4!" },

};

exports.getMessages = function (userid) {
    return users[userid].messages.map(function (mid) {
        return messages[mid];
    })
}

exports.createUser = function (user) {

    users["34"]=user;
    return user;
}

exports.users = users;