
const graphql = require('graphql');
const data = require('./database');


const messagesType = new graphql.GraphQLObjectType({

    name:"Message",
    fields:{
        id:{
            type:graphql.GraphQLInt
        },
        text:{
            type:graphql.GraphQLString
        }
    }

})

const userType = new graphql.GraphQLObjectType({

    name:'User',
    fields:{
        name:{
            type:graphql.GraphQLString
        },
        id:{
            type:graphql.GraphQLString
        },
        messages:{
            type: new graphql.GraphQLList(messagesType),
            resolve(root, args) {
                return data.getMessages(root.id);
            }
        }
    }

});

const messageInputType = new graphql.GraphQLInputObjectType({
    name:"messageInput",
    fields:{
        id:{
            type:graphql.GraphQLInt
        },
        text:{
            type:graphql.GraphQLString
        }
    }
});


const userInputType = new graphql.GraphQLInputObjectType({
    name:"userInput",
    fields:{
        name:{
            type:new graphql.GraphQLNonNull(graphql.GraphQLString)
        },
        id:{
            type:graphql.GraphQLString
        },
        messages:{
            type: new graphql.GraphQLList(messageInputType),

        }
    }
});

const mutationType = new graphql.GraphQLObjectType({

    name:'mutation',
    fields:{
        addUser:{
            type:userType,
            args:{
                user:{
                    type:new graphql.GraphQLNonNull(userInputType)
                }
            },
            resolve(user, args){
                return data.createUser(args.user) ;
            }
        }
    }

});



const queryType = new graphql.GraphQLObjectType({

    name:'Query',
    fields:{
        user:{
            type:userType,
            args:{
                id:{
                    type:graphql.GraphQLString
                }
            },
            resolve:function(root, args) {
                return data.users[args.id];
            }
        }
    }

});

const schema = new graphql.GraphQLSchema({
    query:queryType,
    mutation: mutationType
})

module.exports = schema;